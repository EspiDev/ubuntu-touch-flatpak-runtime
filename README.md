# Ubuntu Touch Flatpak Runtime

This is an attempt to package the Ubuntu Touch libraries as flatpak runtime,
with the goal of running Ubuntu Touch apps.
While some apps already work fine, the overall state is experimental.

## Install

The repository is built on GitLab ci. You can add it to your flatpak installation using:
```
flatpak remote-add --no-gpg-verify ubports https://gitlab.com/JBBgameich/ubuntu-touch-flatpak-runtime/-/jobs/artifacts/master/raw/repo/?job=apps
```

You can find out which applications exist using `flatpak remote-ls ubports`, and install them as usual with flatpak.
